#/*
# * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
# * All right reserved.
# *
# * File: samyptron.pro
# *
# * This file is part of Samyptron.
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# */

# Qt Settings
TEMPLATE = subdirs
CONFIG = ordered

samyptron.subdir = src
samyptron.depends =
SUBDIRS += samyptron
