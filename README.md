Samyptron Studio
==============

Samyptron Studio is a lightweight application to create installer using the Qt Installer Framework from The Qt Company Ltd. And Samyptron Studio is written in Qt self. :-)

How to install?
---------------

Get the source from GIT and prepare usage:

    git clone https://bitbucket.org/ChMaster/samyptron.git

Get dependies:

* [Qt 5.4+](http://www.qt.io/download-open-source/)
* [QScintilla 2](http://www.riverbankcomputing.co.uk/software/qscintilla/intro)
* [Qt Installer Framework 1.5](http://download.qt.io/official_releases/qt-installer-framework/1.5.0/)

Compile and run (OS X / Linux):

    cd samyptron
    qmake -r "CONFIG+=release"
    make --jobs=8 all
    cd build/bin
    ./samyptron

Compile and run (Windows):

    cd samyptron
    qmake -r "CONFIG+=release"
    jom /A /S /J 8
    cd build/bin
    ./samyptron

What's inside?
------------------

The Samyptron Studio includes 3rdparty libraries such:

  * [Qt Property Browser](http://code.qt.io/cgit/qt-solutions/qt-solutions.git/)
  * [Qt Single Application](http://code.qt.io/cgit/qt-solutions/qt-solutions.git/)

Enjoy!