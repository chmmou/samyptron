Based on %{SPS_QT_VERSION}%

From revision %{SPS_VCS_VERSION}%

Copyright © 2015 Alexander Saal. All rights reserved.

The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

Acknowledgments:
Qt Toolkit, Copyright © 2014 Digia Plc and/or its subsidiary(-ies).
Qt Installer Framework, Copyright © 2015, The Qt Company Ltd.
Qt Property Browser (Qt Solutions), Copyright © 2013 Digia Plc and/or its subsidiary(-ies).
Qt Single Application (Qt Solutions), Copyright © 2013 Digia Plc and/or its subsidiary(-ies).
QScintilla 2, Copyright © 2014 Riverbank Computing Limited.
Sphere 1.4 Icon Set, Copyright © 2014 Achim Karsch.
