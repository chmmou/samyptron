/*
 * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of Samyptron.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "project.h"

#include "config.h"

SAMYPTRON_USE_NAMESPACE

Config *config = NULL;
SamyptronProjects samyProjects = SamyptronProjects();

Project::Project(QObject *parent)
  : QThread(parent) {

  config = new Config(this);
}

Project::~Project() {

  if (config) {
    delete config;
  }

  if (samyProjects.size() > 0) {
    qDeleteAll(samyProjects);
    samyProjects.clear();
  }
}

void Project::createProject(const SamyptronProject *project) {

  Q_UNUSED(project)
}

void Project::updateProject(const SamyptronProject *project) {

  Q_UNUSED(project)
}

const SamyptronProject *Project::deleteProject(const QString &id) {

  Q_UNUSED(id)

  return NULL;
}

const SamyptronProjects Project::projects() {
  return samyProjects;
}

void Project::save(const SamyptronProjects &projects) {

  if (projects.size() > 0) {

    // TODO: Add functionality to save all projects

    if (samyProjects.size() > 0) {
      qDeleteAll(samyProjects);
      samyProjects.clear();
    }

    samyProjects << projects;
  }
}
