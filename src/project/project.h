/*
 * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of Samyptron.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAMYPTRON_PROJECT_H
#define SAMYPTRON_PROJECT_H

#include "global.h"

#include <QThread>

SAMYPTRON_BEGIN_NAMESPACE

class Config;

/**
 * @brief The Project class
 */
class Project : public QThread {

    Q_OBJECT

  public:
    /**
     * @brief Default constructor
     */
    Project(QObject *parent = 0);

    /**
     * @brief Default de-constructor
     */
    ~Project();

  public:
    /**
     * @brief Create a new project
     *
     * @param project The project to create
     */
    void createProject(const SamyptronProject *project);

    /**
     * @brief Update a existing project
     *
     * @param project The project to update
     */
    void updateProject(const SamyptronProject *project);

    /**
     * @brief Delete a existing project
     *
     * @param id  The ID for the existing project
     *
     * @return Returns the deleted project
     */
    const SamyptronProject *deleteProject(const QString &id);

    /**
     * @brief Get all existing project
     *
     * @return Returns all projects; otherwise a empty project list
     */
    const SamyptronProjects projects();

  private:
    void save(const SamyptronProjects &projects);
};

SAMYPTRON_END_NAMESPACE

#endif // SAMYPTRON_PROJECT_H
