/*
 * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * File: global.h
 *
 * This file is part of Samyptron.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAMYPTRON_GLOBAL_H
#define SAMYPTRON_GLOBAL_H

#include <QList>
#include <QString>

#ifdef SAMYPTRON_DEBUG
#include <QDebug>
#define debugMsg(x) qDebug() << x
#else
#define debugMsg(x)
#endif

#define SAMYPTRON_NAMESPACE samyptron
#define SAMYPTRON_USE_NAMESPACE using namespace SAMYPTRON_NAMESPACE;
#define SAMYPTRON_BEGIN_NAMESPACE namespace SAMYPTRON_NAMESPACE {
#define SAMYPTRON_END_NAMESPACE }

#define SPS_VERSION QString("0.1.1.5-%1").arg(QT_STRINGIFY(SPS_REV_VERSION))
#define SPS_QT_VERSION QT_VERSION_STR
#define SPS_BUILD_DATE_STR QT_STRINGIFY(SPS_BUILD_DATE)
#define SPS_BUILD_DATE_TIME_STR QT_STRINGIFY(SPS_BUILD_DATE_TIME)

/** Based on format Qt 5.4.1 */
#define SPS_BASED_ON QString("Qt %1").arg(SPS_QT_VERSION)

/** Built on format: Apr 13 2015 at 01:01:01 AM */
#define SPS_BUILD_ON QString(QObject::tr("%1 at %2")).arg(SPS_BUILD_DATE_STR).arg(SPS_BUILD_DATE_TIME_STR)

struct InstallerMeta {};

struct InstallerPackageComponent {

};

struct InstallerData {

};

struct InstallerConfig {

    InstallerMeta *meta;
    InstallerData *data;
};

struct InstallerPackage {

    InstallerMeta *meta;
    InstallerData *data;
};

struct SamyptronProject {

  QString id;
  QString name;

  InstallerConfig *config;
  InstallerPackage *package;

};
typedef QList<SamyptronProject *> SamyptronProjects;

/**
 *
 */
namespace settings {

  struct Environment {

  };

  struct TextEditor {

  };

  struct Designer {

  };

  struct Settings {

      Environment *se;
      TextEditor *ste;
      Designer *sd;
  };
}
#endif // SAMYPTRON_GLOBAL_H
