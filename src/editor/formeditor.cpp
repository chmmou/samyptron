/*
 * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of Samyptron.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "formeditor.h"

#include <QSettings>

#ifdef DESIGNER_LIBRARY
#include <QDesignerComponents>
#endif

#include <QtDesigner>
#include <QPluginLoader>

QDesignerFormEditorInterface *m_formeditor = 0;
QDesignerIntegrationInterface *m_integration = 0;
QDesignerFormWindowManagerInterface *m_fwm = 0;

SAMYPTRON_USE_NAMESPACE

class SettingsManagerPrivate
    : public QDesignerSettingsInterface {

  public:
    SettingsManagerPrivate();
    virtual ~SettingsManagerPrivate();

    virtual void beginGroup(const QString &prefix);
    virtual void endGroup();

    virtual bool contains(const QString &key) const;
    virtual void setValue(const QString &key, const QVariant &value);
    virtual QVariant value(const QString &key, const QVariant &defaultValue = QVariant()) const;
    virtual void remove(const QString &key);

  private:
    QSettings *m_settings;
    QString addPrefix(const QString &name) const;

};

SettingsManagerPrivate::SettingsManagerPrivate()
  : m_settings(0) {

  m_settings = new QSettings(QSettings::NativeFormat, QSettings::UserScope, "asaal", "Samyptron");
}

SettingsManagerPrivate::~SettingsManagerPrivate() {
  if (m_settings) {
    m_settings->sync();
    delete m_settings;
  }
}

void SettingsManagerPrivate::beginGroup(const QString &prefix) {
  m_settings->beginGroup(addPrefix(prefix));
}

void SettingsManagerPrivate::endGroup() {
  m_settings->endGroup();
}

bool SettingsManagerPrivate::contains(const QString &key) const {
  return m_settings->contains(addPrefix(key));
}

void SettingsManagerPrivate::setValue(const QString &key, const QVariant &value) {
  m_settings->setValue(addPrefix(key), value);
}

QVariant SettingsManagerPrivate::value(const QString &key, const QVariant &defaultValue) const {
  return m_settings->value(addPrefix(key), defaultValue);
}

void SettingsManagerPrivate::remove(const QString &key) {
  m_settings->remove(addPrefix(key));
}

QString SettingsManagerPrivate::addPrefix(const QString &name) const {

  QString result = name;
  if (m_settings->group().isEmpty()) {
    result.prepend(QLatin1String("SamyptronDesigner"));
  }
  return result;
}

FormEditor::FormEditor(QWidget *parent)
  : QWidget(parent) {

#ifdef DESIGNER_LIBRARY
  m_formeditor = QDesignerComponents::createFormEditor(0);
  m_formeditor->setTopLevel(this);
  m_formeditor->setSettingsManager(new SettingsManagerPrivate());

  QDesignerComponents::createTaskMenu(m_formeditor, this);
  QDesignerComponents::initializePlugins(m_formeditor);
  QDesignerComponents::initializeResources();

  m_fwm = m_formeditor->formWindowManager();

  QList<QObject*> plugins = QPluginLoader::staticInstances();
  plugins += m_formeditor->pluginInstances();
  foreach (QObject *plugin, plugins) {
      if (QDesignerFormEditorPluginInterface *formEditorPlugin = qobject_cast<QDesignerFormEditorPluginInterface*>(plugin)) {
          if (!formEditorPlugin->isInitialized())
              formEditorPlugin->initialize(m_formeditor);
      }
  }

  QDesignerWidgetBoxInterface *wb = QDesignerComponents::createWidgetBox(m_formeditor, 0);
  wb->setWindowTitle(tr("Widget Box"));
  wb->setObjectName(QLatin1String("WidgetBox"));
  m_formeditor->setWidgetBox(wb);

  QDesignerObjectInspectorInterface *oi = QDesignerComponents::createObjectInspector(m_formeditor, 0);
  oi->setWindowTitle(tr("Object Inspector"));
  oi->setObjectName(QLatin1String("ObjectInspector"));
  m_formeditor->setObjectInspector(oi);

  QDesignerPropertyEditorInterface *pe = QDesignerComponents::createPropertyEditor(m_formeditor, 0);
  pe->setWindowTitle(tr("Property Editor"));
  pe->setObjectName(QLatin1String("PropertyEditor"));
  m_formeditor->setPropertyEditor(pe);
#endif
}

FormEditor::~FormEditor() {

  if (m_formeditor) {
    delete m_formeditor;
  }

  if (m_integration) {
    delete m_integration;
  }

  if (m_fwm) {
    delete m_fwm;
  }
}
