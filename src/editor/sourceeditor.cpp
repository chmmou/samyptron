/*
 * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of Samyptron.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Project Headers */
#include "editor/sourceeditor.h"
#include "editor/private/sourceeditor_p.h"
#include "settings/settings.h"

/** Qt Headers */
#include <QFile>
#include <QFileInfo>
#include <QFont>
#include <QFontMetrics>

/** QScintilla 2 Headers */
#include <Qsci/qscilexerjavascript.h>
#include <Qsci/qscilexerxml.h>
#include <Qsci/qsciapis.h>

SAMYPTRON_USE_NAMESPACE

SourceEditorPrivate::SourceEditorPrivate(const SourceEditor *parent)
  : d_Ptr(const_cast<SourceEditor *>(parent)),
    m_fileName(QString::null) {

  QFont sourceEditor("Courier", 12);
  sourceEditor.setFixedPitch(true);

  d_Ptr->setFont(sourceEditor);

  QFontMetrics fontmetrics = QFontMetrics(d_Ptr->font());
  int marginWidth = fontmetrics.width(QString::number(d_Ptr->lines())) + 6;

  d_Ptr->setMarginsFont(d_Ptr->font());
  d_Ptr->setMarginWidth(0, marginWidth);
  d_Ptr->setMarginLineNumbers(0, true);
  d_Ptr->setMarginsBackgroundColor(QColor("#cccccc"));
  d_Ptr->setCaretLineVisible(true);
  d_Ptr->setCaretLineBackgroundColor(QColor("#ffe4e4"));

  QsciScintilla::FoldStyle state = static_cast<QsciScintilla::FoldStyle>((!d_Ptr->folding()) * 5);
  if (!state) {
    d_Ptr->foldAll(false);
  }
  d_Ptr->setFolding(state);

  d_Ptr->setUtf8(true);
  d_Ptr->setTabIndents(true);
  d_Ptr->setTabWidth(2);

#ifdef SAMYPTRON_DEBUG
  loadFile(QString::null);
#endif

}

SourceEditorPrivate::~SourceEditorPrivate() {

  if (d_Ptr) {
    d_Ptr = 0;
  }
}

void SourceEditorPrivate::loadFile(const QString &fileName) {

  if (fileName.isEmpty()) {
#ifndef SAMYPTRON_DEBUG
    return;
#endif
  }

  m_fileName = QString(fileName);

  QString fileEntries = QString::null;

#ifdef SAMYPTRON_DEBUG
  QFile file(":/samyptron/example/package");
  m_fileName = "package.xml";
#else
  QFile file(m_fileName);
#endif

  if (file.open(QIODevice::Text | QIODevice::ReadOnly)) {
    fileEntries = file.readAll();
    file.close();

    QsciLexer *lexer = 0;
    QFileInfo fileInfo(file);

#ifdef SAMYPTRON_DEBUG
    QString fileSuffix = "xml";
#else
    QString fileSuffix = fileInfo.suffix();
#endif

    if (fileSuffix.contains("xml")) {

      lexer = new QsciLexerXML(d_Ptr);
      ((QsciLexerXML *)lexer)->setDefaultFont(d_Ptr->font());
    }
    else if (fileSuffix.contains("js") || fileSuffix.contains("qs")) {

      lexer = new QsciLexerJavaScript(d_Ptr);
      ((QsciLexerJavaScript *)lexer)->setDefaultFont(d_Ptr->font());
      ((QsciLexerJavaScript *)lexer)->setFoldComments(true);
    }

    prepareLexer(lexer, fileSuffix);

    d_Ptr->setText(fileEntries);

    fileEntries = QString::null;
  }
  else {
    debugMsg(file.errorString());
  }
}

void SourceEditorPrivate::saveFile(const QString &fileName) {

  Q_UNUSED(fileName)
}

void SourceEditorPrivate::prepareLexer(QsciLexer *lexer, const QString &fileSuffix) {

  bool isApiLoaded = false;
  QsciAPIs *api = new QsciAPIs(lexer);
  connect(api, SIGNAL(apiPreparationCancelled()), this, SLOT(apiPreparationCancelled()));
  connect(api, SIGNAL(apiPreparationStarted()), this, SLOT(apiPreparationStarted()));
  connect(api, SIGNAL(apiPreparationFinished()), this, SLOT(apiPreparationFinished()));

  QString fileName = QFileInfo(m_fileName).baseName();

  if (fileSuffix.contains("xml")) {

    /**
     * Use the right api component for autocompletion
     */

    if (fileName.contains("config")) {
      isApiLoaded = api->load(QString(":/samyptron/api/config"));
    }
    else if (fileName.contains("package")) {
      isApiLoaded = api->load(QString(":/samyptron/api/package"));
    }
  }
  else if (fileSuffix.contains("js") || fileSuffix.contains("qs")) {
    // TODO Add autocompletion api for JS/QS scripting
  }

  if (isApiLoaded) {
    api->prepare();
  }
}

void SourceEditorPrivate::apiPreparationCancelled() {

  QsciAPIs *api = qobject_cast<QsciAPIs *>(sender());
  Q_UNUSED(api)

}

void SourceEditorPrivate::apiPreparationStarted() {

  QsciAPIs *api = qobject_cast<QsciAPIs *>(sender());
  Q_UNUSED(api)
}

void SourceEditorPrivate::apiPreparationFinished() {

  QsciAPIs *api = qobject_cast<QsciAPIs *>(sender());
  api->savePrepared("C:/Users/asaal/package.qsci");
  d_Ptr->setAutoCompletionThreshold(1);
  d_Ptr->setAutoCompletionSource(QsciScintilla::AcsAPIs);
  d_Ptr->setLexer(api->lexer());
}

SourceEditor::SourceEditor(QWidget *parent)
  : QsciScintilla(parent),
    d_ptr(0) {

  d_ptr = new SourceEditorPrivate(this);
}

SourceEditor::~SourceEditor() {

  delete d_ptr;
}

void SourceEditor::loadFile(const QString &fileName) {

  d_ptr->loadFile(fileName);
}

void SourceEditor::saveFile(const QString &fileName) {

  d_ptr->saveFile(fileName);
}
