#/*
# * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
# * All right reserved.
# *
# * File: src.pri
# *
# * This file is part of Samyptron.
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# */

include(3rdparty/qtpropertybrowser/qtpropertybrowser.pri)
include(3rdparty/qtsingleapplication/qtsingleapplication.pri)

##
# Includes & Depend path
##
INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

INCLUDEPATH += $$PWD/3rdparty/qtpropertybrowser
DEPENDPATH += $$PWD/3rdparty/qtpropertybrowser
INCLUDEPATH += $$PWD/3rdparty/qtsingleapplication
DEPENDPATH += $$PWD/3rdparty/qtsingleapplication

INCLUDEPATH += $$PWD/3rdparty/qtpropertybrowser
DEPENDPATH += $$PWD/3rdparty/qtpropertybrowser
INCLUDEPATH += $$PWD/3rdparty/qtsingleapplication
DEPENDPATH += $$PWD/3rdparty/qtsingleapplication

INCLUDEPATH += $$PWD/about
DEPENDPATH += $$PWD/about

INCLUDEPATH += $$PWD/config
DEPENDPATH += $$PWD/config

INCLUDEPATH += $$PWD/editor
DEPENDPATH += $$PWD/editor

INCLUDEPATH += $$PWD/editor/private
DEPENDPATH += $$PWD/editor/private

INCLUDEPATH += $$PWD/packages
DEPENDPATH += $$PWD/packages

INCLUDEPATH += $$PWD/project
DEPENDPATH += $$PWD/project

INCLUDEPATH += $$PWD/samyptron
DEPENDPATH += $$PWD/samyptron

INCLUDEPATH += $$PWD/settings
DEPENDPATH += $$PWD/settings

##
# Headers, Source & Ui files
##
HEADERS += global.h
SOURCES += main.cpp

HEADERS += about/about.h
SOURCES += about/about.cpp
FORMS += about/about.ui

HEADERS += config/config.h
SOURCES += config/config.cpp

HEADERS += editor/private/sourceeditor_p.h
HEADERS += editor/sourceeditor.h
SOURCES += editor/sourceeditor.cpp

HEADERS += editor/formeditor.h
SOURCES += editor/formeditor.cpp

HEADERS += packages/packages.h
SOURCES += packages/packages.cpp

HEADERS += project/project.h
SOURCES += project/project.cpp

HEADERS += samyptron/samyptron.h
SOURCES += samyptron/samyptron.cpp
FORMS += samyptron/samyptron.ui

HEADERS += settings/settings.h
SOURCES += settings/settings.cpp
FORMS += settings/settings.ui
