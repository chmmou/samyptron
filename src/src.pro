#/*
# * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
# * All right reserved.
# *
# * File: src.pro
# *
# * This file is part of Samyptron.
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# */

# Project Settings
PARENT_DIR = ..
INSTALL_PATH = /usr/local
BUILD_DIR = $${PARENT_DIR}/build
RESOURCES_DIR = $${PARENT_DIR}/res

macx {
  TARGET_PREFIX = Samyptron
}
else {
  unix {
    CONFIG += debug
    TARGET_PREFIX = samyptron
  }
  else {
    TARGET_PREFIX = Samyptron
  }
}

# Qt Settings
TEMPLATE = app
DESTDIR = $${BUILD_DIR}/bin
TARGET = $${TARGET_PREFIX}
RESOURCES += $${RESOURCES_DIR}/samyptron.qrc
CONFIG += qt thread warn_on qscintilla2 silent
QT += network xml designer

# Specify designer settings
DEFINES += DESIGNER_LIBRARY
QT += designercomponents-private

# Qt 5 support
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# Special settings
APP_BUNDLE = $${TARGET}
macx {
  APP_BUNDLE = $${BUILD_DIR}/bin/$${TARGET}.app/Contents/MacOS/$${TARGET}
} else {
  unix {
    APP_BUNDLE = $${BUILD_DIR}/bin/$${TARGET}
  }
}

# Some version defines
unix {
  DEFINES += SPS_REV_VERSION='$(shell git rev-list --full-history --all --abbrev-commit | head -1)'
  DEFINES += SPS_BUILD_DATE='"$(shell date \'+%b %d %Y\')"'
  DEFINES += SPS_BUILD_DATE_TIME='"$(shell date \'+%r\')"'
}

win32 {
  DEFINES += SPS_BUILD_DATE='"$$system(powershell get-date -format \'MMM dd yyyy\')"'
  DEFINES += SPS_BUILD_DATE_TIME='"$$system(powershell get-date -format \'T\')"'

  GIT_PATH='$$system($$PWD/$${RESOURCES_DIR}\git_path.cmd)'
  !isEmpty(GIT_PATH) {
    DEFINES += SPS_REV_VERSION='"$$system($${GIT_PATH}\bin\git.exe rev-list --full-history --all --abbrev-commit --max-count=1)"'
  }
  else {
    DEFINES += SPS_REV_VERSION="1"
  }
}

# Config settings
CONFIG(debug, debug|release) {

  RCC_DIR = $${BUILD_DIR}/$${TARGET}/debug/rcc
  MOC_DIR += $${BUILD_DIR}/$${TARGET}/debug/moc
  OBJECTS_DIR += $${BUILD_DIR}/$${TARGET}/debug/obj
  UI_DIR += $${BUILD_DIR}/$${TARGET}/debug/ui

  DEFINES += SAMYPTRON_DEBUG

  macx {
    QMAKE_POST_LINK = install_name_tool -change \
                        libqscintilla2_debug.11.dylib \
                        $$[QT_INSTALL_LIBS]/libqscintilla2_debug.11.dylib \
                        $${APP_BUNDLE}
  }

} else {

  RCC_DIR = $${BUILD_DIR}/$${TARGET}/release/rcc
  MOC_DIR += $${BUILD_DIR}/$${TARGET}/release/moc
  OBJECTS_DIR += $${BUILD_DIR}/$${TARGET}/release/obj
  UI_DIR += $${BUILD_DIR}/$${TARGET}/release/ui

  DEFINES -= SAMYPTRON_DEBUG

  macx {
    QMAKE_POST_LINK = strip -S $${APP_BUNDLE} && \
                      install_name_tool -change libqscintilla2.11.dylib $$[QT_INSTALL_LIBS]/libqscintilla2.11.dylib $${APP_BUNDLE}
  }
  else {
    unix {
      QMAKE_POST_LINK = strip -s $${APP_BUNDLE}
    }
  }
}

# Windows settings
win32 {
  DEFINES += QSCINTILLA_DLL
  CONFIG += embed_manifest_dll
  CONFIG += embed_manifest_exe
  RC_FILE = $${RESOURCES_DIR}/samyptron.rc
  CONFIG -= console
  CONFIG -= flat
}

macx {
  CONFIG(release, release|debug) {
    QMAKE_CFLAGS_RELEASE = $$QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO
    QMAKE_CXXFLAGS_RELEASE = $$QMAKE_CXXFLAGS_RELEASE_WITH_DEBUGINFO
    QMAKE_OBJECTIVE_CFLAGS_RELEASE =  $$QMAKE_OBJECTIVE_CFLAGS_RELEASE_WITH_DEBUGINFO
    QMAKE_LFLAGS_RELEASE = $$QMAKE_LFLAGS_RELEASE_WITH_DEBUGINFO
  }

  ICON = $${RESOURCES_DIR}/$${TARGET}.icns
  QMAKE_INFO_PLIST = $${RESOURCES_DIR}/$${TARGET}-Info.plist
  OTHER_FILES += $${RESOURCES_DIR}/$${TARGET}-Info.plist
}

unix {
  OTHER_FILES += $${PARENT_DIR}/README.md
}


include(src.pri)
