/*
 * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of Samyptron.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "samyptron.h"
#include "formeditor.h"
#include "sourceeditor.h"
#include "settings.h"
#include "about.h"

#include <QWidget>
#include <QMessageBox>

SAMYPTRON_USE_NAMESPACE

Samyptron::Samyptron(QWidget *parent, Qt::WindowFlags flags)
  : QMainWindow(parent, flags) {

#ifdef Q_OS_WIN
  setWindowIcon(QIcon(":/samyptron/appIcon"));
#endif

  setupUi(this);

  debugMsg(SPS_VERSION);
  debugMsg(SPS_BASED_ON);
  debugMsg(SPS_BUILD_ON);
}

Samyptron::~Samyptron() {

}

void Samyptron::messageReceived(const QString &message) {

  QMessageBox::information(this, tr("Samyptron"), message, QMessageBox::Ok);
}
