/*
 * Copyright © 2015 Alexander Saal <alexander.saal@chm-projects.de>
 * All right reserved.
 *
 * This file is part of Samyptron.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAMYPTRON_H
#define SAMYPTRON_H

#include "global.h"
#include "ui_samyptron.h"

#include <QMainWindow>

SAMYPTRON_BEGIN_NAMESPACE

  class Samyptron : public QMainWindow,
                    private Ui::UISamyptronMainWindow {

      Q_OBJECT

    public:
      explicit Samyptron(QWidget *parent = 0, Qt::WindowFlags flags = 0);
      ~Samyptron();

    public Q_SLOTS:
      void messageReceived(const QString &message);

  };

SAMYPTRON_END_NAMESPACE

#endif // SAMYPTRON_H
